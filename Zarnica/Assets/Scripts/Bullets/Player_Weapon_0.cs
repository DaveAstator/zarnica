﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Player_Weapon_0 : MonoBehaviour {


	public int level = 0;


	public float Shootdelay=0.002f; //in seconds
	private
	float delay=0f;
	int isfiring=0;

	private GameObject ActiveBullet;
	private int DirX, DirY;

	// Use this for initialization
	void Start () {
		ActiveBullet = null;
		DirX = 0;
		DirY = 0;
	}

	public void Zero(){
		isfiring = 0;
		delay = 0f;
		level = 0;
		Shootdelay=0.002f;
	}
	// Update is called once per frame
	public void PowerUp(int uplvl){
		level += 1;
	}

	void Update () {
		delay -= Time.unscaledDeltaTime;

		//============= firing
		if (Input.GetKey (KeyCode.Z)) {
			isfiring = 1;
		} else
			isfiring = 0;// (Input.GetKeyUp(KeyCode.Space))
		/*{
			isfiring = 0;
		}
*/
		//============ Left

		int a = 0;
		int b = 0;
		if (Input.GetKey(KeyCode.LeftArrow))
		{
			a = 1;
		}

		//============ right
		if (Input.GetKey(KeyCode.RightArrow))
		{
			b = -1;

		}
		DirX = a + b;

		// ====== up
		a=0;
		b = 0;
		if (Input.GetKey(KeyCode.UpArrow))
		{
			a = 1;
		}

		if (Input.GetKey(KeyCode.DownArrow))
		{
			b = -1;
		}
		DirY = a + b;




		float z=0f;
		if (DirY == 0)
			z = 90 * DirX;
		else if (DirY == 1)
			z = 45 * DirX;
		else if (DirY == -1)
			z = 180 - (45 * DirX);

    	Quaternion Rot = Quaternion.Euler (new Vector3 (0, 0, z));


		if (isfiring == 1) {
			if (delay < 0) {
				if (ActiveBullet == null) {

					if (level == 0) {
						ActiveBullet = (GameObject)Instantiate (Resources.Load ("prefabs/weapons/Weapon_0"));
					}
					else if (level == 1) {
						ActiveBullet = (GameObject)Instantiate (Resources.Load ("prefabs/weapons/Weapon_0_LV1"));
					}
					else if (level == 2) {
						ActiveBullet = (GameObject)Instantiate (Resources.Load ("prefabs/weapons/Weapon_0_LV2"));
					}
					else if (level >= 3) {
						ActiveBullet = (GameObject)Instantiate (Resources.Load ("prefabs/weapons/Weapon_0_LV3"));
					}

						ActiveBullet.transform.position = this.transform.position;
						ActiveBullet.transform.rotation = Rot;//this.transform.rotation;
					ActiveBullet.GetComponent<Rigidbody2D> ().AddForce (ActiveBullet.transform.up * 1600 * ActiveBullet.GetComponent<Rigidbody2D> ().mass * 2f);// who knows but 2f corrects for px/sec
					Quaternion zer = Quaternion.Euler (new Vector3 (0, 0, 0));
					ActiveBullet.transform.rotation = zer;
						ActiveBullet.transform.parent = Camera.main.transform;


				}

				delay = Shootdelay + delay;
			}

			//	AudioSource snd = gameObject.AddComponent<AudioSource> ();
			//	snd.pitch = Random.Range(0.9f,1.1f);
			//	snd.PlayOneShot ((AudioClip)Resources.Load ("sound/shoot1"));
			}
		if (delay < 0) delay = 0;


		}
}
