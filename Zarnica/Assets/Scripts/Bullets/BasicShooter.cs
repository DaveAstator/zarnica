﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BasicShooter : MonoBehaviour {

	public float Interval=1f;
	public float RandomDeviation=0f;
	public bool ModByShootSpeed=false;
	private float mainticker=0f;


	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		float IntervalMod = Interval;
		if (ModByShootSpeed)
			IntervalMod = IntervalMod / GlobalDifficulty.K_EnmShootSpeed;
		
		if (mainticker > IntervalMod) {
			Shoot ();
			print ("SHOOTED!");
			mainticker=mainticker-IntervalMod;
		}

		mainticker += Time.unscaledDeltaTime;
	}

	public virtual void Shoot(){
	// empty for virtualisation
	
	}

}
