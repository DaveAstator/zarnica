﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Phased_Bullet : MonoBehaviour {
	public float Shift=0f;
	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		float t = GetComponent<BaseValues> ().AgeTime+Shift;
		GetComponent<BaseValues> ().Vel.x = (Mathf.Sin (t * 3) * (1 - Mathf.Abs (Mathf.Cos (t * 5))))*5;
		GetComponent<BaseValues> ().Vel.y = -1.8f; 
	}
}
