﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Player_Main_Gun : MonoBehaviour {
	public 
	int level = 0;


	public float Shootdelay=0.3f; //in seconds
	private
	float delay=0f;

	int isfiring=0;
	//public int powerlevel = 0;
	// Use this for initialization
	void Start () {

	}

	public void Zero(){
		isfiring = 0;
		delay = 0f;
		level = 0;
		Shootdelay=0.3f;
	}
	// Update is called once per frame
	public void GetMainBoost(int am){
		level += am;
		//if (level >1)
			//Shootdelay = Mathf.Max (0.1f, Shootdelay-0.03f);
	}

	void Update () {
		delay -= Time.unscaledDeltaTime;

		if (Input.GetKey (KeyCode.Space)) {
			isfiring = 1;
		} else
			isfiring = 0;



		if (isfiring == 1) {
			if (delay < 0) {

			
				if (level == 0 ) {
					GameObject Mgunb = (GameObject)Instantiate (Resources.Load ("prefabs/weapons/main_gun_0a"));
					Mgunb.transform.position = this.transform.position+ this.transform.up *0.1f;
					Mgunb.transform.rotation = this.transform.rotation;
					Mgunb.transform.parent = Camera.main.transform;
				}
				if (level == 1) {
					
					GameObject Mgunb1 = (GameObject)Instantiate (Resources.Load ("prefabs/weapons/MainGun_LV2"));
				//	GameObject Mgunb2 = (GameObject)Instantiate (Resources.Load ("prefabs/weapons/main_gun_0a"));
					Mgunb1.transform.position = this.transform.position;
					Mgunb1.transform.rotation = this.transform.rotation;
					Mgunb1.transform.parent = Camera.main.transform;

				}
				if (level == 2) {

					GameObject Mgunb1 = (GameObject)Instantiate (Resources.Load ("prefabs/weapons/MainGun_LV3"));
					//	GameObject Mgunb2 = (GameObject)Instantiate (Resources.Load ("prefabs/weapons/main_gun_0a"));
					Mgunb1.transform.position = this.transform.position;
					Mgunb1.transform.rotation = this.transform.rotation;
					Mgunb1.transform.parent = Camera.main.transform;

				}
				if (level >= 3) {
					GameObject Mgunb1 = (GameObject)Instantiate (Resources.Load ("prefabs/weapons/MainGun_LV3"));
				//	GameObject Mgunb2 = (GameObject)Instantiate (Resources.Load ("prefabs/weapons/main_gun_0a"));
				//	GameObject Mgunb = (GameObject)Instantiate (Resources.Load ("prefabs/weapons/main_gun_0a"));
					Mgunb1.transform.position = this.transform.position;

					Mgunb1.transform.rotation = this.transform.rotation;

					Mgunb1.transform.parent = Camera.main.transform;

				}


				delay = Shootdelay + delay;
				/*
				AudioSource snd = gameObject.AddComponent<AudioSource> ();
				snd.pitch = Random.Range(0.9f,1.1f);
				snd.PlayOneShot ((AudioClip)Resources.Load ("sound/shoot1"));
				*/
			}
		}
		if (delay < 0) {delay = 0;}


	}
}
