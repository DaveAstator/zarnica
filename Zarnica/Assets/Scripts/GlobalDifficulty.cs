﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GlobalDifficulty : MonoBehaviour {
	[Tooltip("asdasda")]
	public static float K_EnmSpawnAmount=1;
	public static float K_EnmShootSpeed=1;
	public static float K_EnmSpeed=1;
	public static float K_PlrShootAmount=1;
	public static float K_PlrShootDamage=1;

	public float i_EnmSpawnAmount=1;
	public float i_EnmShootSpeed=1;
	public float i_EnmSpeed=1;
	public float i_PlrShootAmount=1;
	public float i_PlrShootDamage=1;
	public float Boss_increment=0f;


	private static int TotalShots;
	private static int TotalHits;
	private static int TotalKills;
	private static float PlayerLifetime=0f;

	public GameObject Player=null;

	public static GlobalDifficulty GDinstance=null;


	// Use this for initialization
	void Awake () {
		//SFXout.clip = clip2;
		//SFXout.Play();
		//SFXout.clip = clip1;
		//SFXout.Play();
		if (GDinstance == null) {
			GDinstance = this;
		} else if (GDinstance != this)
			Destroy (gameObject);

		DontDestroyOnLoad (this);
		Player = GameObject.Find ("zarnica");
	}



	public void GetHitResult (int res){
		if (res == 1)
			TotalHits += 1;
		else if (res == 2) {
			TotalHits += 1;
			TotalKills += 1;
		}

		TotalShots+=1;
	}

	public void PlayerDied(){
		PlayerLifetime = 0f;
	}

	public float GetPowerLevel(){
		return (Player.GetComponent<PLR_GunManager> ().Level + Player.GetComponent<Player_Main_Gun> ().level)/2f;
	
	}

	void Start () {
		K_EnmSpawnAmount = i_EnmSpawnAmount;
		K_EnmShootSpeed = i_EnmShootSpeed;
		K_EnmSpeed = i_EnmSpeed;
		K_PlrShootAmount = i_PlrShootAmount;
		K_PlrShootDamage = i_PlrShootDamage;

	}

	// Update is called once per frame
	void Update () {
		K_EnmShootSpeed = 0.6f + Mathf.Min((GetPowerLevel()*GetPowerLevel() / 6f),2f);
		K_EnmSpawnAmount= 1f + (GetPowerLevel() / 3f);
		// K_EnmSpeed = 1f + Mathf.Min(PlayerLifetime*PlayerLifetime / 16200f,1.6f); // maximum after 3 minutes;

		PlayerLifetime += Time.unscaledDeltaTime;

	}
}
