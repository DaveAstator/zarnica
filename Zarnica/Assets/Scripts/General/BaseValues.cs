﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BaseValues : MonoBehaviour {

	private float Cx,Cy,Cz;
	public float AgeTime = 0.0f;
	 //float vx,vy,vz;
	public Vector3 Vel;
	public Transform CalcTransform;
	public int Rotate=1;
	public List<int> testlist;
	public int HP;
	public int SCORE;
	public Vector3 LocalMovPosition;
	public Quaternion LocalMovRotation;
	public bool DisableAnimation = false;
	public AudioClip HitSound = null;
	public bool UseRB = false;

	public static float RoundToNearestPixel(float unityUnits)
	{
		//float valueInPixels = (Screen.height / (viewingCamera.orthographicSize * 2)) * unityUnits;
		float ors= Screen.height / 2.0f / 100.0f;
		float valueInPixels = (Screen.height / (ors * 2)) * unityUnits;
		valueInPixels = Mathf.Round(valueInPixels);
		float adjustedUnityUnits = valueInPixels / (Screen.height / (ors * 2));
		return adjustedUnityUnits;
	}
	public void OnDrawGizmos(){
		this.transform.position = new Vector3 (
			Mathf.Round(RoundToNearestPixel(this.transform.position.x)/0.16f)*0.16f,
			Mathf.Round(RoundToNearestPixel(this.transform.position.y)/0.16f)*0.16f, 
			this.transform.position.z);
	}
	public void ApplyDamage(int damage) {
		HP -= damage;
		//print ("Taken damage!");
		if (HitSound != null)
			SoundManager.instance.PlaySFX (HitSound);
			//GameObject.Find ("goSoundManager").SendMessage ("PlaySFX", HitSound);
		if (HP < 1) {
			GlobalVars.gvinstance.AddToScore (SCORE);
			HP = 0;
		}

	}
	void Start(){
		if (DisableAnimation) this.GetComponent<Animator> ().speed = 0.0f;
		//MovDummy=new GameObject();
		//LocalMovTransform = MovDummy.transform;
		//LocalMovTransform.position = this.transform.localPosition;
		//LocalMovTransform.rotation = this.transform.localRotation;
		//LocalMovTransform.localScale = this.transform.localScale;
		LocalMovRotation=this.transform.localRotation;
		LocalMovPosition=this.transform.localPosition;
		Cx = this.transform.position.x;
		Cy = this.transform.position.y;
		Vel = new Vector3 (0, 0, 0);
		this.transform.position = new Vector3 (RoundToNearestPixel(this.transform.position.x),RoundToNearestPixel(this.transform.position.y), this.transform.position.z);
	}

	void FixedUpdate(){
		if (UseRB && GetComponent<Rigidbody2D> () != null) {
			//if (Rotate == 0) this.transform.localRotation = Quaternion.Euler (0f, 0f, 0f);
		/*
			this.transform.localPosition = LocalMovPosition;
			//LocalMovTransform.position += LocalMovTransform.right * Vel.x;
			//LocalMovTransform.position += LocalMovTransform.up * Vel.y;
			if (Rotate == 0) {
				this.transform.localRotation = LocalMovRotation;
				this.transform.position += this.transform.right * Vel.x * Time.fixedUnscaledDeltaTime;
				this.transform.position += this.transform.up * Vel.y * Time.fixedUnscaledDeltaTime;
				this.transform.localRotation = Quaternion.Euler (0f, 0f, 0f);
			} else if (Rotate == 1) {
				this.transform.position += this.transform.right * Vel.x * Time.fixedUnscaledDeltaTime;
				this.transform.position += this.transform.up * Vel.y * Time.fixedUnscaledDeltaTime;
			}

			LocalMovPosition = this.transform.localPosition;
			//		this.transform.localPosition=LocalMovTransform.position;

			//this.transform.rotation = Quaternion.Euler (0f,0f,0f);

			//		Cx = LocalMovTransform.position.x;
			//		Cy = LocalMovTransform.position.y;
*/
			Cx = this.transform.position.x;
			Cy = this.transform.position.y;
			//BTV.Cx += this.transform.right * Vel.x;
			//BTV.Cy += this.transform.up * Vel.y;
			//BTV.Cz += BTV.vz;


			this.transform.position = new Vector3 (RoundToNearestPixel (Cx), RoundToNearestPixel (Cy), this.transform.position.z);
		}
	
	}

	void Update(){
		AgeTime += Time.unscaledDeltaTime;

		if (!UseRB) {
			this.transform.localPosition = LocalMovPosition;
			//LocalMovTransform.position += LocalMovTransform.right * Vel.x;
			//LocalMovTransform.position += LocalMovTransform.up * Vel.y;
			if (Rotate == 0) {
				this.transform.localRotation = LocalMovRotation;
				this.transform.position += this.transform.right * Vel.x * Time.unscaledDeltaTime;
				this.transform.position += this.transform.up * Vel.y * Time.unscaledDeltaTime;
				this.transform.localRotation = Quaternion.Euler (0f, 0f, 0f);
			} else if (Rotate == 1) {
				this.transform.position += this.transform.right * Vel.x * Time.unscaledDeltaTime;
				this.transform.position += this.transform.up * Vel.y * Time.unscaledDeltaTime;
			}

			LocalMovPosition = this.transform.localPosition;
			//		this.transform.localPosition=LocalMovTransform.position;
		
			//this.transform.rotation = Quaternion.Euler (0f,0f,0f);

//		Cx = LocalMovTransform.position.x;
//		Cy = LocalMovTransform.position.y;
			Cx = this.transform.position.x;
			Cy = this.transform.position.y;
			//BTV.Cx += this.transform.right * Vel.x;
			//BTV.Cy += this.transform.up * Vel.y;
			//BTV.Cz += BTV.vz;


			this.transform.position = new Vector3 (RoundToNearestPixel (Cx), RoundToNearestPixel (Cy), this.transform.position.z);
		}
	}

}
