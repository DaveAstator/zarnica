﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MovAhead_RB : MonoBehaviour {
	// Value is in pixels per second

	// value is auto corrected for px per second.
	public float value=-0.05f;
	public int phase=1;
	private
	BaseValues BV;
	// Use this for initialization
	void Start () {
		// 1/0.01 = 100 frames per second for physics
		// applying 1600*20=32000 units.
		// 32000 / 100fps / 10 mass = 32 smth / frame.
		// 32 /frame
		// 32*100= 3200 px /sec.
		// 

		// 100 frames / sec
		// applying 1 / 100 = 
		// 16 / 10 = 1.6
		//

		this.GetComponent<Rigidbody2D> ().AddForce (this.transform.up * value * this.GetComponent<Rigidbody2D> ().mass * 2f);// who knows but 2f corrects for px/sec
//		BV = this.GetComponent<BaseValues>();	
	}

	// Update is called once per frame
	void Update () {

//		BV.Vel.y = value;
		//this.transform.localPosition += this.transform.up * value;//new Vector3(this.transform.position.x,this.transform.position.y-0.05f,this.transform.position.z);

	}
}