﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Scn_Camcontrol : MonoBehaviour {
	public float TimeOut = 1f;
	public int KillTimeoutMultiply = 1;

	public bool useScreensPerSecond=false;
	public float TargetVelocity = 0.03f;
	//public GameObject TargetDummy;
	public float PosBias=0.5f;
	public bool Triggered=false;



	private float ticker = 0f;
	private float startvel=0f;
	private float deltavel=0f;
	// Use this for initialization
	void Start () {
		ticker = 0f;
	}
	
	// Update is called once per frame
	void Update () {
		if ((this.transform.position.y < Camera.main.transform.position.y + Screen.height/200f )&&
			( !Triggered)&&
			(Mathf.Abs(this.transform.position.y - Camera.main.transform.position.y)<10)
		
		){
			Triggered = true;
			startvel = Camera.main.GetComponent<Cam_test_SCR> ().FlySpeed;
			print ("Triggered wityh startvel:" +startvel.ToString());
			if (useScreensPerSecond) TargetVelocity = (Screen.height / 100.0f) * TargetVelocity;
		//	print (TargetVelocity);
			deltavel = TargetVelocity - startvel;

		}

		if (Triggered) {
			if (ticker >= TimeOut)
				ticker = TimeOut;
			
		//	print ("ticker:" + ticker.ToString());
			Camera.main.GetComponent<Cam_test_SCR> ().FlySpeed = startvel + deltavel * (ticker / TimeOut);


			if (ticker >= TimeOut) {
				Camera.main.GetComponent<Cam_test_SCR> ().FlySpeed = TargetVelocity;
				Destroy (gameObject, TimeOut * KillTimeoutMultiply - TimeOut);
				this.enabled = false;
			}
			//Destroy (this);
			
			ticker += Time.unscaledDeltaTime;

		}

	}
}
