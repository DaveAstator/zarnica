﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Collision_Damage : MonoBehaviour {

	public string DieOnTag;
	public int TransferPerCollision = 9000;
	public int EmitDamage = 0;

	public bool DieSilent=false;
	public bool DieOnHp=true;
	public bool DieOnPowerLoss=true;
	public bool Unreduceable = false;
	public List<string> SpawnOnDeath;
	[Tooltip("-1 = destroy, >=0 dont destroy and set new frame.")]
	public int CorpseFrame=-1;

	void Update(){
		if (DieOnHp && this.GetComponent<BaseValues> ().HP < 1)
			Die ();
	}

	void OnTriggerEnter2D(Collider2D col){
		int exitdmg = Mathf.Min (EmitDamage, TransferPerCollision);
		if (!Unreduceable)
			EmitDamage -= exitdmg;
		
		if (exitdmg > 0) {
			col.gameObject.SendMessage ("ApplyDamage", exitdmg);
		}

		if ((EmitDamage<1) && (DieOnPowerLoss)) Die();


		
		//this.transform.position = new Vector3 (this.transform.position.x + 0.04f, this.transform.position.y, this.transform.position.z);
/*
		print ("col detec");
		if (!DieSilent) {
			GameObject EXP = (GameObject)Instantiate (Resources.Load ("prefabs/effects/EffExplosion"));
			EXP.transform.position = this.transform.position;//this.transform.position;
			EXP.transform.localScale = this.transform.localScale;
			EXP.transform.SetParent (Camera.main.transform);
		}

		
			Destroy (gameObject);
		//}
		*/	

		if (DieOnHp && this.GetComponent<BaseValues> ().HP < 1)
			Die ();


	}
	private void Die(){
		if (!DieSilent) {
			GameObject EXP = (GameObject)Instantiate (Resources.Load ("prefabs/effects/EnemyExplosion"));
			EXP.transform.position = this.transform.position;//this.transform.position;
			EXP.transform.localScale = this.transform.localScale;
			EXP.transform.SetParent (Camera.main.transform);
		}
		Destroy (gameObject);
	}

}
