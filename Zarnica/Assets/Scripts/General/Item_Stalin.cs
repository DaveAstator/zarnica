﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Item_Stalin : MonoBehaviour {

	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		
	}

	void OnTriggerEnter2D(Collider2D col){
		if (GetComponent<BaseValues> ().HP < 1) {
			GameObject EXP = (GameObject)Instantiate (Resources.Load ("prefabs/items/White_Hole"));
			EXP.transform.position = this.transform.position;
			//EXP.transform.localScale = this.transform.localScale;
			EXP.transform.SetParent (Camera.main.transform);
			GetComponent<BoxCollider2D> ().enabled = false;
		}
	}
}
