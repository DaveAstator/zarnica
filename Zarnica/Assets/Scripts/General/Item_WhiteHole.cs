﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Item_WhiteHole : MonoBehaviour {

	private bool triggered=false;
	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void OnTriggerEnter2D(Collider2D col){
		GameObject SND = (GameObject)Instantiate (Resources.Load ("prefabs/effects/WhiteHoleSound"));
		GameObject EXP = (GameObject)Instantiate (Resources.Load ("prefabs/effects/WhiteScreen"));
		EXP.transform.position = Camera.main.transform.position;
		//EXP.transform.localScale = this.transform.localScale;
		EXP.transform.SetParent(Camera.main.transform);
		triggered = true;
		Destroy (gameObject);
	}
}
