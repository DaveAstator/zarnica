﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SoundStartPlayer : MonoBehaviour {

	public AudioClip clip;

	//private AudioSource SP;
	// Use this for initialization
	void Start () {
		SoundManager.instance.PlaySFX(clip);

	}
	
	// Update is called once per frame
	void Update () {
		
	}
}
