﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DecorScript : MonoBehaviour {

	// Use this for initialization
	public Sprite Corpse;
	public static float RoundToNearestPixel(float unityUnits)
	{
		//float valueInPixels = (Screen.height / (viewingCamera.orthographicSize * 2)) * unityUnits;
		float ors= Screen.height / 2.0f / 100.0f;
		float valueInPixels = (Screen.height / (ors * 2)) * unityUnits;
		valueInPixels = Mathf.Round(valueInPixels);
		float adjustedUnityUnits = valueInPixels / (Screen.height / (ors * 2));
		return adjustedUnityUnits;
	}
	public void OnDrawGizmos(){
		this.transform.position = new Vector3 (
			Mathf.Round(RoundToNearestPixel(this.transform.position.x)/0.16f)*0.16f,
			Mathf.Round(RoundToNearestPixel(this.transform.position.y)/0.16f)*0.16f, 
			this.transform.position.z);
		this.transform.rotation = Quaternion.Euler (
			this.transform.rotation.eulerAngles.x,
			this.transform.rotation.eulerAngles.y,
			Mathf.Round(this.transform.rotation.eulerAngles.z /15f)*15f );
	}


	void Start () {
	//	LayerMask = "Decor";
		gameObject.layer=17;
		this.transform.position = new Vector3 (
			RoundToNearestPixel(this.transform.position.x),
			RoundToNearestPixel(this.transform.position.y), 
			10);
	}
	void OnTriggerEnter2D(Collider2D col){
		GetComponent<SpriteRenderer> ().sprite = Corpse;
	}

	// Update is called once per frame
	void Update () {
		
	}
}
