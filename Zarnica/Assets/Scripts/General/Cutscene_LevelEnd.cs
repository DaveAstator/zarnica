﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Cutscene_LevelEnd : MonoBehaviour {
	private GameObject plr;
	private bool Triggered=false;
	// Use this for initialization
	void Start () {

	}
	
	// Update is called once per frame
	void Update () {

		if ((this.transform.position.y < Camera.main.transform.position.y) && (!Triggered)) {
			print ("Endscene triggered");
		Camera.main.GetComponent<Cam_test_SCR> ().FlySpeed = 0f;
			Camera.main.GetComponent<Cam_test_SCR> ().enabled = false;
		plr = GameObject.Find ("zarnica");
		GameObject EXP = (GameObject)Instantiate (Resources.Load ("prefabs/effects/zrEndEmitter"));
		EXP.transform.position = new Vector3(plr.transform.position.x,Camera.main.transform.position.y-(Screen.height / 200f),plr.transform.position.z-0.01f);//this.transform.position;
		//EXP.transform.parent=Camera.main.transform;
		//EXP.transform.localPosition = new Vector3 (EXP.transform.localPosition.x, (-Screen.height / 200f) + (32f / 100f), EXP.transform.localPosition.z);
		plr.GetComponent<SpriteRenderer> ().enabled = false;
		plr.GetComponent<PLR_DeathScript> ().InvulnerableTime = 900f;
			Triggered = true;
		}
	}
}
