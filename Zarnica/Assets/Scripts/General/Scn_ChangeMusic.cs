﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Scn_ChangeMusic : MonoBehaviour {
	public string MusicNodeName;
	public AudioClip MusicTrack;
	public float FadeTimeSec = 1f;
	public bool noloop=false;
	private bool triggered=false;
	private bool changed = false;
	private bool fadeaway=false;
	private bool fadein=false;
	private float del=0f;
	private AudioSource MP;
	// Use this for initialization
	void Start () {
		MP = SoundManager.instance.MusicOut; //GameObject.Find ("MusicPlayer").GetComponent<AudioSource> ();
	}
	
	// Update is called once per frame
	void Update () {
		if ((this.transform.position.y < Camera.main.transform.position.y) && !triggered) {
			print ("Music change!");
			fadeaway = true;
			del = FadeTimeSec;
			triggered = true;
		}
		if (fadeaway) {
			del -= Time.unscaledDeltaTime;
			MP.volume = del / FadeTimeSec;
			if (del < 0) {
				MP.Stop ();
				if (MusicTrack != null) {
					if (noloop)
						MP.loop = false;
					MP.volume = 1f;
					MP.clip = MusicTrack;//(GameObject.Find(MusicNodeName).GetComponent<AudioSource> ().clip);
					MP.Play ();
				}
				fadein = true;
				fadeaway = false;
			
			}
		}
		if (fadein) {
			Destroy (this);
		}

	}
}
