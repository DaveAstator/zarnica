﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Kill_Offcamera : MonoBehaviour {
	public 
	float killdistance=0.1f;
	public float KillAgeTime=-1f;
	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {

		if ((this.transform.position.y > Camera.main.transform.position.y+(Screen.height / 200f) + killdistance) ||
			(this.transform.position.y < Camera.main.transform.position.y-(Screen.height / 200f) - killdistance) ||
			(this.transform.position.x > Camera.main.transform.position.x+(Screen.width / 200f) + killdistance) ||
			(this.transform.position.x < Camera.main.transform.position.x-(Screen.width / 200f) - killdistance))
//		if (Vector2.Distance (this.transform.position, Camera.main.transform.position) > Screen.width / 200f + killdistance) {
			die();
		
		if (this.GetComponent<BaseValues>() != null)
		if (this.GetComponent<BaseValues>().AgeTime*Mathf.Sign(KillAgeTime) > Mathf.Abs(KillAgeTime)) die();

	}
	public void die(){
		for (int i=0;i<transform.childCount;i++){
			Destroy (transform.GetChild (i).gameObject);
		}
		Destroy (gameObject);
	}
}


