﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Item_SecondaryPowerUp : MonoBehaviour {

	public int ItemValue=0;
	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		
	}

	void OnTriggerEnter2D(Collider2D col){
		col.gameObject.SendMessage ("GetBonus", ItemValue);
		print ("sent bonus" + col.gameObject.name);
		GetComponent<AudioSource> ().Play ();
		GetComponent<BoxCollider2D> ().enabled = false;
		GetComponent<SpriteRenderer> ().enabled = false;
		Destroy (gameObject,3);


		//}
	}
}

