﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Turret_Triangle_ShootScript2 : BasicShooter {

	public
	int SpawnShift = 1;
//	public float SpawnDelay = 24;
	public int Burstcount =5;
	public string ObjName="prefabs/weapons/triangle";
	public bool AttachToCam =false;
	public bool FireEnabled=false;
	// Use this for initialization
	void Start () {

	}

	// Update is called once per frame
	public void StartShoot(){
		FireEnabled = true;
	}
	public void StopShoot(){
		FireEnabled = false;
	}

	/*void Update () {

		Update();
	}*/

	public override void Shoot(){
		if (FireEnabled) {

		print ("indeed!");
				float StartAngle = Random.Range (-180f / Burstcount, 180f / Burstcount);
				for (int i = 0; i < Burstcount; i++) {

					GameObject EXP = (GameObject)Instantiate (Resources.Load (ObjName));
					EXP.transform.position = new Vector3 (this.transform.position.x, this.transform.position.y, this.transform.position.z - 2);//this.transform.position;

					EXP.transform.localRotation = Quaternion.Euler (0f, 0f, StartAngle + i * 360f / Burstcount); //this.transform.rotation;
					EXP.transform.localScale = this.transform.lossyScale;
					if (AttachToCam)
						EXP.transform.SetParent (Camera.main.transform);
				}

		}
	}

}
