using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class beh_eFighter01 : MonoBehaviour {
	public float value=-0.02f;
	public int phase=1;
	private BaseValues BV;
	private int signx=1;
	private int signy=1;
	Animator a; 

	// Use this for initialization
	void Start () {
		BV = this.GetComponent<BaseValues>();	
		if (this.transform.position.x > GameObject.Find ("zarnica").transform.position.x) {
			signx = -1;
			this.GetComponent<SpriteRenderer> ().flipX = true;
		}
		if (this.transform.position.y<GameObject.Find ("zarnica").transform.position.y) {
			signy = -1;
			this.GetComponent<SpriteRenderer> ().flipY = true;
		}
		BV.Vel.y = value*signy;

		a = this.GetComponent<Animator> ();
	//	a.playbackTime = 0.0f;
		a.speed = 0.0f;
		a.Play ("z0s", -1, 0.0f);

//		print (a.GetCurrentAnimatorClipInfoCount(0));
	}

	// Update is called once per frame
	void Update () {
		BV.Vel.y -= 0.009f*signy*GlobalDifficulty.K_EnmSpeed;
		BV.Vel.x += 0.027f*signx*GlobalDifficulty.K_EnmSpeed;


		if ((BV.Vel.x) < -2.245) {
			a.Play ("z0s", -1, 0.9f);
		}
		else if ((BV.Vel.x) > 2.245) {
			a.Play ("z0s", -1, 0.9f);
		}
		else if ((BV.Vel.x) > 1.325) {
			a.Play ("z0s", -1, 0.5f);
		}
		else if ((BV.Vel.x) < -1.325) {
			a.Play ("z0s", -1, 0.5f);
		}
		else a.Play ("z0s", -1, 0.0f);



		//this.transform.localPosition += this.transform.up * value;//new Vector3(this.transform.position.x,this.transform.position.y-0.05f,this.transform.position.z);

	}
}
