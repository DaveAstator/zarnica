﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PLR_DeathScript : MonoBehaviour {
	public float RespawnTime=2;
	public float delaytime;
	public int isdead=0;
	public float InvulnerableTime=0f;
	BaseValues BV;

	void Start () {
		BV = GetComponent<BaseValues>();
		delaytime = RespawnTime;
	}

	void OnTriggerEnter2D(Collider2D col){
			col.gameObject.SendMessage ("ApplyDamage", 1);

	}
	public void GetMainBoost(int am){
		InvulnerableTime = 1f;
	}
	public void GetBonus(int am){
		InvulnerableTime = 1f;
	}

	// Update is called once per frame
	void Update () {
		if (InvulnerableTime < 0.01) {
		this.GetComponent<SpriteRenderer> ().color = Color.HSVToRGB (1f,0f,1f);
			if (BV.HP <= 0 && isdead == 0) {
				GameObject EXP = (GameObject)Instantiate (Resources.Load ("prefabs/effects/EffPlayerExplosion"));
				EXP.transform.position = this.transform.position;//this.transform.position;
				EXP.transform.localScale = this.transform.localScale;
				EXP.transform.SetParent (Camera.main.transform);

				this.GetComponent<SpriteRenderer> ().enabled = false;
				this.GetComponent<Rigidbody2D> ().simulated = false;
				this.GetComponent<Player_Main_Gun> ().enabled = false;
				this.GetComponent<PLR_GunManager> ().Die ();
				//this.GetComponent<PLR_Control> ().enabled = false;

//			BV.Vel = Vector3.zero;
				delaytime = RespawnTime;
				isdead = 1;
				print ("must die!");
				GlobalVars.gvinstance.AssignLives (-1);
				GlobalDifficulty.GDinstance.PlayerDied ();

				//Destroy (gameObject);

			}
			delaytime -= Time.unscaledDeltaTime * isdead;

			if (delaytime < 0) {
				InvulnerableTime = 1.5f;
				this.GetComponent<SpriteRenderer> ().enabled = true;
				this.GetComponent<Rigidbody2D> ().simulated = true;
				this.GetComponent<Player_Main_Gun> ().enabled = true;
				this.GetComponent<Player_Main_Gun> ().Zero ();
				this.GetComponent<PLR_GunManager> ().Reset ();
				//this.GetComponent<PLR_Control> ().enabled = true;
				//this.transform.localPosition= new Vector3(0f,0f,0f);
				BV.LocalMovPosition = new Vector3 (0f, -Screen.height / 400f, 0f);
//			BV.Vel = Vector3.zero;

				BV.HP = 1;
				delaytime = RespawnTime;
				isdead = 0;

			}

		} 
		else {
			BV.HP = 1;
			InvulnerableTime -= Time.unscaledDeltaTime;
			this.GetComponent<SpriteRenderer> ().color = Color.HSVToRGB (((InvulnerableTime * 1000*4) % 1000) / 1000, 0.2f, 1f);
		}
	}

}
