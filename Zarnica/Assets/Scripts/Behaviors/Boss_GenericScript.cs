﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Boss_GenericScript : MonoBehaviour {
	public enum States {bInactive, bBattle, bDead, bToScore, bToEnd, bOver,bFailed};

		private float sumHP = 2f;
	private bool isDead=false;
	public States state = States.bInactive;
	public float TimeOut=30f;
	public bool SwitchMusic=true;
	private float DeathToScore=2f;
	private float ScoreToRun=1f;
	private float ticker = 0f;
	private float TOticker=0f;

	public AudioClip Bossmusic;
	private AudioClip EntryMusic;

//	public GameObject CamEndController = null;
	// Use this for initialization
	void Awake(){
		state = States.bInactive;
//		GetComponent<Scn_Camcontrol> ().enabled = false;
	}
	void Start () {
	//	CamEndController.transform.parent=null;
		sumHP = 2f;
		GetComponent<Scn_Camcontrol> ().enabled = false;

		state = States.bInactive;
	}
	
	// Update is called once per frame
	void Update () {
		// ================= detect camera stop.======================
		if ((Camera.main.GetComponent<Cam_test_SCR> ().FlySpeed < 0.01) && 
			(state == States.bInactive) &&
			(Camera.main.transform.position.y > this.transform.position.y - Screen.height/200f) &&
			(Mathf.Abs(Camera.main.transform.position.y - this.transform.position.y)<10)
		) {
			if (SwitchMusic) {
				EntryMusic = SoundManager.instance.MusicOut.clip;
				SoundManager.instance.MusicOut.Stop ();
				SoundManager.instance.MusicOut.clip = Bossmusic;
				SoundManager.instance.MusicOut.volume = 1f;
				SoundManager.instance.MusicOut.Play ();
			}
				state = States.bBattle;

			for (int i=0;i<transform.childCount;i++){
				transform.GetChild(i).SendMessage("Activate");
			}

		}

		// ==================== count boss hp
		sumHP = 0f;
		for (int i=0;i<transform.childCount;i++){
    			
		sumHP += Mathf.Max(transform.GetChild(i).gameObject.GetComponent<BaseValues> ().HP,0);
		//	print (i.ToString()+ "hp of this:" +transform.GetChild(i).gameObject.GetComponent<BaseValues> ().HP.ToString());
		//	print (sumHP);
		}

		//=============== tick the timer
		if (state == States.bBattle) {
			TOticker += Time.unscaledDeltaTime;
			GameObject txt = GameObject.Find ("BossTimeText");
			txt.GetComponent<Text> ().text = "TIME: "+((int)(TimeOut - TOticker)).ToString ();
			// catch time failure
			if (TOticker>=TimeOut) {
				this.GetComponent<Scn_Camcontrol> ().enabled = true;
				ticker = 0;
				state = States.bFailed;
				if (SwitchMusic)
				SoundManager.instance.MusicOut.Stop ();

				for (int i=0;i<transform.childCount;i++){
					transform.GetChild(i).SendMessage("Enrage");
				}
				txt.GetComponent<Text> ().text = " ";

			}

		}
		//=========================== Boss is killed ==================
		if ((sumHP < 1f)&& (state==States.bBattle)) {
			GameObject EXP = (GameObject)Instantiate (Resources.Load ("prefabs/effects/WhiteScreen"));
			EXP.transform.position = Camera.main.transform.position;
			//EXP.transform.localScale = this.transform.localScale;
			EXP.transform.SetParent(Camera.main.transform);

			print ("boss is dead ((");
			print ("children:" + transform.childCount.ToString());
			print (sumHP);
			this.GetComponent<BaseValues> ().ApplyDamage (1000);
			isDead = true;
			ticker = 0f;
			SoundManager.instance.MusicOut.Stop ();
			
			state = States.bToScore;
			//for (int i = 0; i < transform.childCount - 1; i++) {
			//	transform.GetChild (0).gameObject.transform.parent = null;
			//}
			//Destroy(gameObject,20);


		}

		else if (state == States.bToScore) {
			ticker += Time.unscaledDeltaTime;
			if (ticker > DeathToScore) {
				state = States.bToEnd;
				ticker = 0;
			}

		}

		else if (state == States.bToEnd) {
			GameObject txt = GameObject.Find ("BossTimeText");
			txt.GetComponent<Text> ().text = " ";
			ticker += Time.unscaledDeltaTime;
			if (ticker > DeathToScore) {
				this.GetComponent<Scn_Camcontrol> ().enabled = true;
				ticker = 0;
				state = States.bOver;
				if (SwitchMusic) SoundManager.instance.MusicOut.clip = EntryMusic;

					SoundManager.instance.MusicOut.Play ();

			}

		}

}
}
