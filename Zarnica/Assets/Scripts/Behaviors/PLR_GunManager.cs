﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PLR_GunManager : MonoBehaviour {
	public GameObject AttachedGun = null;

	public int ActiveWeapon=0;
	public int Level = 0;
	// Use this for initialization
	void Start () {
		this.Reset ();
	}
	
	// Update is called once per frame
	void Update () {
		
	}

	public void GetBonus(int num){

		print ("GotBonus" + num.ToString());
		if (num != ActiveWeapon) {
			ActiveWeapon = num;
			Destroy (AttachedGun.gameObject);
			AttachedGun = (GameObject)Instantiate (Resources.Load ("prefabs/weapons/Weapon_" + num.ToString () + "_Controller"));
			AttachedGun.transform.SetParent (this.transform);
			AttachedGun.transform.localPosition = Vector3.zero;
			AttachedGun.GetComponent<Player_Weapon_0> ().level = 0;
			Level = 0;
		} else {
			AttachedGun.SendMessage ("PowerUp", 1);
			Level += 1;
		}

	}

	public void Die()
	{
		Destroy (AttachedGun.gameObject);
	}

	public void Reset(){
		ActiveWeapon = 0;
		AttachedGun=(GameObject)Instantiate (Resources.Load ("prefabs/weapons/Weapon_0_Controller"));
		AttachedGun.transform.SetParent (this.transform);
		AttachedGun.transform.localPosition = Vector3.zero;
		AttachedGun.GetComponent<Player_Weapon_0> ().level = 0;
		Level = 0;
	}

}
