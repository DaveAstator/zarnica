﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AssistRespawner : MonoBehaviour {
	//public List<Vector3> Ranges;
	public Vector2 H_Range;
	[Tooltip("1=top 0=bottom")]
	public float V_Range;
	public string PrefabName;
	public Vector2 TimeInterval = new Vector2 (2f, 2f);
	[Tooltip("Attach children to camera on spawn")]
	public bool AttachToCam = false;

	public bool DieOnCount=false;
	public int DieCountValue=1;
	public bool DieOnTimeout=false;
	public bool WaitForCamera=true;


	private int spawncount=1;
	public float delay=0f;
	public bool activated=false;

	//private nextrange
	// Use this for initialization
	private float w = Screen.width / 100f;
	private float h = Screen.height / 100f;
	void Start () {
		delay = Random.Range(TimeInterval.x,TimeInterval.y);
		if (!WaitForCamera) {
			this.transform.SetParent (Camera.main.transform);
			activated = true;
		}

		//int userange=Mathf.Round(Random.Range(0,Ranges.Count-1));
	}
	
	// Update is called once per frame
	void Update () {

		if (activated) {
			delay -= Time.unscaledDeltaTime;
			//print (delay);
			if (delay <= 0f) {
			
				GameObject SPWN = (GameObject)Instantiate (Resources.Load (PrefabName));

				float x = Random.Range (H_Range.x * w, H_Range.y * w) - (w / 2.0f);
				float y = (V_Range * h) - (h / 2.0f);
				//SPWN.transform.position = new Vector3(Camera.main.transform.position.x+x,Camera.main.transform.position.y+y, this.transform.position.z); // this.transform.position;//this.transform.position;
				SPWN.transform.position = new Vector3 (Camera.main.transform.position.x + x, Camera.main.transform.position.y + y, this.transform.position.z); // this.transform.position;//this.transform.position;
				SPWN.transform.rotation = this.transform.rotation;
				//SPWN.transform.localScale = this.transform.localScale;
				spawncount += 1;
				if (AttachToCam)
					SPWN.transform.SetParent (Camera.main.transform);
				delay = Random.Range (TimeInterval.x, TimeInterval.y) + (delay % TimeInterval.x);
				if (DieOnCount && spawncount > DieCountValue*GlobalDifficulty.K_EnmSpawnAmount)
					Destroy (gameObject);

			}
			// 1st range is [0]
		} else if ((this.transform.position.y < Camera.main.transform.position.y + Screen.height / 200f)&&
			!activated &&
			Mathf.Abs(this.transform.position.y - Camera.main.transform.position.y) <10
		)
			activated = true;


	}
}
