﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Bonus_Main_Pickup : MonoBehaviour {

	public int BoostAmount=1;
	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		
	}

	void OnTriggerEnter2D(Collider2D col){
		//this.transform.position = new Vector3 (this.transform.position.x + 0.04f, this.transform.position.y, this.transform.position.z);
		this.GetComponent<AudioSource>().Play();

		//if (col.tag == DieOnTag) {

		col.gameObject.SendMessage("GetMainBoost", BoostAmount);
		GetComponent<BoxCollider2D> ().enabled = false;
		GetComponent<SpriteRenderer> ().enabled = false;
		Destroy (gameObject,3);
		Destroy (this);
		//}
	}
}
