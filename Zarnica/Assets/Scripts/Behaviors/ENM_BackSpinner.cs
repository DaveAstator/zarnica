﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ENM_BackSpinner : MonoBehaviour {


	public int state =0;
	public float Kat = 1f; //katalizator
	private float rotated=0f;
	public int signx=0;
	public int signy=0;
	private float rotsign = 1;
	// Use this for initialization
	void Start () {
		if (this.transform.position.x > Camera.main.transform.position.x)
			signx = -1;
		else signx=1;

		if (this.transform.position.y < Camera.main.transform.position.y)
			signy = 1;
		else signy=-1;

		//if (state == 0) {

		//}
	}

	// Update is called once per frame
	void Update () {
		if (this.state == 0) {
			GetComponent<BaseValues> ().Vel.y = 2f*Kat*GlobalDifficulty.K_EnmSpeed;
			if ((this.transform.position.x * signx > Camera.main.transform.position.x * signx + (Screen.width / 500f)) ||
			    (this.transform.position.y * signy > Camera.main.transform.position.y * signy + (Screen.height / 500f))) {
				float dx = this.transform.position.x - Camera.main.transform.position.x;
				float dy = this.transform.position.y - Camera.main.transform.position.y;

				/*if (dx <= 0 && dy <= 0)
					rotsign = -1;
				else if (dx > 0 && dy < 0)
					rotsign = 1;
				else if (dx <= 0 && dy > 0)
					rotsign = 1;
				else if (dx > 0 && dy > 0)
					rotsign = 1;
					*/
				Vector3 testpos = (this.transform.position + this.transform.right * 0.02f);
				Vector2 tp2d=new Vector2 (testpos.x,testpos.y);
				Vector2 realp2d=new Vector2 (this.transform.position.x, this.transform.position.y);
				Vector2 campos = new Vector2 (Camera.main.transform.position.x, Camera.main.transform.position.y);
				if (Vector2.Distance(testpos,campos) > Vector2.Distance(realp2d,campos))
					rotsign = 1;
					else rotsign=-1;


					state = 1;
				GetComponent<Kill_Offcamera> ().enabled = true;
			}
		} else if (this.state == 1) {
			
			this.transform.localRotation=Quaternion.Euler(0,0,this.transform.localRotation.eulerAngles.z+90f*Time.unscaledDeltaTime*GlobalDifficulty.K_EnmSpeed*rotsign);
			rotated += 90f * Time.unscaledDeltaTime*GlobalDifficulty.K_EnmSpeed;
				if (rotated > 360-90) this.state=2;
		}

	}
}
