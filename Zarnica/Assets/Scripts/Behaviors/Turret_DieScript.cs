﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Turret_DieScript : MonoBehaviour {
	public float DeathFrameTime=0.99f;
//	public AudioClip DeathSound;
	BaseValues BV;
	// Use this for initialization
	void Start () {
		BV = this.GetComponent<BaseValues>();
	}


	// Update is called once per frame
	void Update () {

		if (BV.HP<=0) {
			GameObject EXP = (GameObject)Instantiate (Resources.Load ("prefabs/effects/Expl_Turret"));
			EXP.transform.position = new Vector3(this.transform.position.x,this.transform.position.y,this.transform.position.z-0.01f);//this.transform.position;
			EXP.transform.localScale = this.transform.localScale;

			//EXP.transform.SetParent (Camera.main.transform);

			//if (DeathFrameTime <0) Destroy (gameObject);
			this.GetComponent<SPWN_continious> ().enabled = false;
			this.GetComponent<Animator> ().Play ("zTurret01_dead", -1, 0.0f);
			this.GetComponent<Turret_DieScript> ().enabled = false;
			this.GetComponent<BoxCollider2D> ().enabled = false;
		//	this.transform.SetParent (null);
		}

	}
}
