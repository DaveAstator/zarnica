﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ITM_SecBonusHolder : MonoBehaviour {
	public int WeapNum=0;

	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		
		
	}
	void OnTriggerEnter2D(Collider2D col){
		if (GetComponent<BaseValues> ().HP < 1) {
			GameObject EXP = (GameObject)Instantiate (Resources.Load ("prefabs/items/ITM_Weapon_"+WeapNum.ToString()));
			EXP.transform.position = new Vector3 (this.transform.position.x, this.transform.position.y, this.transform.position.z - 1);//this.transform.position;
			EXP.transform.localRotation = Quaternion.Euler(this.transform.rotation.x,this.transform.rotation.y,this.transform.rotation.z);
			EXP.transform.localScale = this.transform.lossyScale;
			EXP.transform.SetParent (Camera.main.transform);

			GameObject EXP1 = (GameObject)Instantiate (Resources.Load ("prefabs/effects/Expl_Turret"));
			EXP1.transform.position = new Vector3(this.transform.position.x,this.transform.position.y,this.transform.position.z-0.01f);//this.transform.position;
			EXP1.transform.localScale = this.transform.localScale;


			Destroy (gameObject);
		}
			
	}
}
