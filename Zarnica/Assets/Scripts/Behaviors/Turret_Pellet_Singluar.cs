﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Turret_Pellet_Singluar : MonoBehaviour {

	public float OffsetTime=0f;
	public float TranSpeed = 1f;
	public float ClosedTime=3f;
	public float ColHideBias=0.5f;
	public float OpeningTime=0.5f;
	public float OpenTime=2f;
	public float ClosingTime=0.5f;
//	public AnimationClip OpeningAnim;
//	public AnimationClip ClosingAnim;
	private float ticker = 0f;
	public int state =-1; // -1= start 0-closed 1- open 2-rage

	// Use this for initialization
	void Start () {
		GetComponent<Animator> ().SetFloat ("TransitionSpeed", TranSpeed);
		//GetComponent<Animation> ().Play(0);
		//AnimationClip OpeningAnm = this.GetComponent<Animation>()["zTurret_pellet_singular"].clip;
		//print (OpeningAnm.length.ToString () + " anm len");
		 // length/wantedseconds
		//print(OpeningAnim.length.ToString()+"anmspd");

		//reslen = anml * 1/spd
		//1/spd=reslen/anml
		// spd = anml/reslen
//		print (GetComponent<Animation>()[0].ToString());			
	//	Animator anm = GetComponent<Animator> ();
	//	RuntimeAnimatorController ac = anm.runtimeAnimatorController;
	//	anm.


		//GetComponent<Animator> ().speed = OpeningAnim.length;

	}
	// sdf
	// Update is called once per frame

	void OnTriggerEnter2D(Collider2D col){
		col.gameObject.SendMessage ("ApplyDamage", 1);

	}
	public void Activate(){
		state = -1;
		ticker = 0f;
	}

	public void Enrage(){
		ticker = 0f;
		GetComponent<Animator> ().SetTrigger ("OpenUp");
		state = 2;
		ticker = 0f;
	}

	void Update () {
		ticker += Time.unscaledDeltaTime;
		if (state == -2) {
			GetComponent<BoxCollider2D> ().enabled = false;
			this.GetComponent<SPWN_continious> ().enabled = false;
		}
		if (state == -1) {      // closed in standby
			GetComponent<BoxCollider2D> ().enabled = false;
			this.GetComponent<SPWN_continious> ().enabled = false;
			if (ticker >= OffsetTime) {
				state = 0;
				ticker = 0f;
			}
		} else if (state == 0) { // closed waiting for open
			if (ticker >= ColHideBias)
				GetComponent<BoxCollider2D> ().enabled = false;
			this.GetComponent<SPWN_continious> ().enabled = false;
			
			if (ticker >= ClosedTime + ClosingTime) {
				GetComponent<Animator> ().SetTrigger ("OpenUp");
				state = 1;
				ticker = 0f;
			}
		} else if (state == 1) { // open waiting for close
			if (ticker >= OpeningTime - ColHideBias)
				GetComponent<BoxCollider2D> ().enabled = true;
			this.GetComponent<SPWN_continious> ().enabled = true;
			if (ticker >= OpeningTime + OpenTime) {
				GetComponent<Animator> ().SetTrigger ("CloseDown");
				state = 0;
				ticker = 0f;
			}
		} else if (state == 2) { // This turret is enraged!
			GetComponent<BoxCollider2D> ().enabled = true;
			this.GetComponent<SPWN_continious> ().enabled = true;
			this.GetComponent<SPWN_continious> ().SpawnDelay = (this.GetComponent<SPWN_continious> ().SpawnDelay * 0.7f);
			state = 3;
		}


		if (GetComponent<BaseValues>().HP<=0) {
			GameObject EXP = (GameObject)Instantiate (Resources.Load ("prefabs/effects/Expl_Turret"));
			EXP.transform.position = new Vector3(this.transform.position.x,this.transform.position.y,this.transform.position.z-0.01f);//this.transform.position;
			EXP.transform.localScale = this.transform.localScale;
			this.GetComponent<SPWN_continious> ().enabled = false;
			this.GetComponent<Animator> ().SetTrigger ("Die");
			this.GetComponent<BoxCollider2D> ().enabled = false;
			//	this.transform.SetParent (null);
			Destroy(this);
		}

	}
}
