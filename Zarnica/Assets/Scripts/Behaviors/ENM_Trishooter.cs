﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ENM_Trishooter : MonoBehaviour {

	public float SpawnDelay = 36f;
	public
	string ObjName="EnemyRocket1";
	public bool AttachToCam =false;
	private	float Delay;
	private int rotmod;

	GameObject plr;
	// Use this for initialization
	void Start () {
		plr = GameObject.Find ("zarnica");
	}
	
	// Update is called once per frame
	void Update () {
			//Vector3 diff= this.transform.position-(GameObject.Find("zarnica").transform.position);
			//diff.Normalize();
			//float rotz = Mathf.Atan2 (diff.y, diff.x) * Mathf.Rad2Deg;
			//this.transform.rotation = Quaternion.Euler (0f,0f,rotz-90); 
			//Quaternion aimrot = Quaternion.Euler (0f,0f,rotz-90);
		if (plr.transform.position.y > transform.position.y) {
			this.GetComponent<SpriteRenderer> ().flipY = true;
			rotmod = 180;
		} else {
			this.GetComponent<SpriteRenderer> ().flipY = false;
			rotmod = 0;
		}

		
		Delay--;
			if (Delay < 1) {
			
			for (int i = -1; i < 1+1; i++) {
					GameObject EXP = (GameObject)Instantiate (Resources.Load ("prefabs/weapons/triangle"));
					EXP.transform.position = new Vector3 (this.transform.position.x, this.transform.position.y, this.transform.position.z - 1);//this.transform.position;
				EXP.transform.localRotation = Quaternion.Euler(this.transform.rotation.x,this.transform.rotation.y,this.transform.rotation.z+(i*30)+Random.Range(-5f,5f)+rotmod);
					EXP.transform.localScale = this.transform.lossyScale;
					EXP.transform.SetParent (Camera.main.transform);
				}


			Delay = SpawnDelay/(GlobalDifficulty.K_EnmShootSpeed);
			}
			//EXP.transform.SetParent (Camera.main.transform);
			//Destroy(this.gameObject);

		GetComponent<BaseValues> ().Vel.x = Mathf.Sign (this.transform.position.x - plr.transform.position.x) * -0.8f;
		GetComponent<BaseValues> ().Vel.y = -0.6f;


		if (GetComponent<BaseValues> ().HP < 1) {
			GameObject EXP = (GameObject)Instantiate (Resources.Load ("prefabs/effects/EnemyExplosion"));
			EXP.transform.position = this.transform.position;//this.transform.position;
			EXP.transform.localScale = this.transform.localScale;
			EXP.transform.SetParent (Camera.main.transform);
			Destroy (gameObject);
		}
	}
}
