﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Turret_Triangle_ShootScript : MonoBehaviour {

	public
	int SpawnShift = 1;
	public float SpawnDelay = 24;
	public int Burstcount =5;
	public
	string ObjName="prefabs/weapons/triangle";
	public bool AttachToCam =false;
	private
	float Delay;
	public bool FireEnabled=false;
	// Use this for initialization
	void Start () {
		Delay = SpawnDelay;
	}

	// Update is called once per frame
	public void StartShoot(){
		FireEnabled = true;
	}
	public void StopShoot(){
		FireEnabled = false;
	}
	void Update () {
		if (FireEnabled) {
			//if (Mathf.Abs(this.transform.position.y-Camera.main.transform.position.y)<(2.90*SpawnShift)) {

			/* aim at player
			Vector3 diff= this.transform.position-(GameObject.Find("zarnica").transform.position);
			diff.Normalize();
			float rotz = Mathf.Atan2 (diff.y, diff.x) * Mathf.Rad2Deg;
			Quaternion aimrot = Quaternion.Euler (0f,0f,rotz-90);
*/

			//this.transform.rotation = Quaternion.Euler (0f,0f,rotz-90); 

			Delay--;
			if (Delay < 0) {

				float StartAngle = Random.Range (-180f / Burstcount, 180f / Burstcount);
				for (int i = 0; i < Burstcount; i++) {
							
					GameObject EXP = (GameObject)Instantiate (Resources.Load (ObjName));
					EXP.transform.position = new Vector3 (this.transform.position.x, this.transform.position.y, this.transform.position.z - 2);//this.transform.position;

					EXP.transform.localRotation = Quaternion.Euler (0f, 0f, StartAngle + i * 360f / Burstcount); //this.transform.rotation;
					EXP.transform.localScale = this.transform.lossyScale;
					if (AttachToCam)
						EXP.transform.SetParent (Camera.main.transform);
				}
				Delay = SpawnDelay + Delay;
			}

			//EXP.transform.SetParent (Camera.main.transform);
			//Destroy(this.gameObject);
			//	}
		}
	}
}
