﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SoundManager : MonoBehaviour {
	public AudioSource MusicOut;
	public AudioSource SFXout;
	public static SoundManager instance = null;
	public AudioClip Startmusic;
	public AudioClip clip2;

	private float FadeOutTime;
	private float FadeInTime;



	// Use this for initialization
	void Awake () {
		//SFXout.clip = clip2;
		//SFXout.Play();
		//SFXout.clip = clip1;
		//SFXout.Play();

		if (instance == null) {
			instance = this;
		} else if (instance != this)
			Destroy (gameObject);

		DontDestroyOnLoad (this);
		if (Startmusic != null)
			MusicOut.clip = Startmusic;
		MusicOut.loop = true;
	}
	public void PlaySFX (AudioClip theclip)
	{
		SFXout.clip = theclip;
		SFXout.Play();
	}

	public void SwitchMusic (AudioClip theclip, float fadeout, float fadein)
	{
		MusicOut.clip = theclip;
		MusicOut.Play();
	}

	// Update is called once per frame
	void Update () {

	}
}
