﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class GlobalVars : MonoBehaviour {
	public static int PlayerLives;
	public static GlobalVars gvinstance=null;
	public static int TotalScore;

	// Use this for initialization
	void Awake () {
		//SFXout.clip = clip2;
		//SFXout.Play();
		//SFXout.clip = clip1;
		//SFXout.Play();
		if (gvinstance == null) {
			gvinstance = this;
		} else if (gvinstance != this)
			Destroy (gameObject);

		DontDestroyOnLoad (this);


	}

	void Start () {
		PlayerLives = 10;
		GameObject txt = GameObject.Find ("LivesText");
		txt.GetComponent<Text> ().text = "Lives: " + PlayerLives.ToString("D2");
	}
	public void AssignLives(int amount){
		PlayerLives += amount;
		if (PlayerLives < 0) {
			GameObject txt = GameObject.Find ("GameoverText");
			txt.GetComponent<Text> ().enabled = true;
			Destroy(GameObject.Find ("zarnica").gameObject);
		} else {


			GameObject txt = GameObject.Find ("LivesText");
			txt.GetComponent<Text> ().text = "Lives: " + PlayerLives.ToString ();
		}


	}
	public void AddToScore(int amount)
	{
		TotalScore += amount;
		GameObject txt = GameObject.Find ("ScoreText");
		txt.GetComponent<Text> ().text = TotalScore.ToString();
	}

	// Update is called once per frame
	void Update () {
		
	}
}
