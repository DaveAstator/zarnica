﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DieOnHPLoss : MonoBehaviour {

	BaseValues BV;
	// Use this for initialization
	void Start () {
		var BV = GetComponent<BaseValues>();
	}


	// Update is called once per frame
	void Update () {

		if ( GetComponent<BaseValues>().HP<=0) {
			GameObject EXP = (GameObject)Instantiate (Resources.Load ("prefabs/effects/EffPlayerExplosion"));
			EXP.transform.position = this.transform.position;//this.transform.position;
			EXP.transform.localScale = this.transform.localScale;
			EXP.transform.SetParent (Camera.main.transform);

Destroy (gameObject);
		
		}

	}
}
