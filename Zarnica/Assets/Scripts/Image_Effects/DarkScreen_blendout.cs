﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DarkScreen_blendout : MonoBehaviour {
	public float TimeOut=1f;
	public Color DestColor;
	private Color StartColor;
	public float ticker;
	// Use this for initialization
	void Start () {
		StartColor = new Color (1f,1f,1f,1f);
		this.GetComponent<SpriteRenderer> ().color = StartColor;
		 ticker = 0f;
	}
	
	// Update is called once per frame
	void Update () {
		ticker += Time.unscaledDeltaTime;
		this.GetComponent<SpriteRenderer> ().color = Color.Lerp(StartColor,DestColor, (ticker / TimeOut));

	}
}
