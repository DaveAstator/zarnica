﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FadeAway : MonoBehaviour {
	public float TimeOut;
	public float ticker=0f;
	public float InactiveTime = 0f;
	private Color basecol;
	private Color zerocol;
	private int State = 0;
	// Use this for initialization
	void Start () {
		basecol = this.GetComponent<SpriteRenderer> ().color;
		zerocol = new Color (1f, 0f, 0f, 0f);
		this.GetComponent<SpriteRenderer> ().color = zerocol;
	}
	
	// Update is called once per frame
	void Update () {
		if (ticker > InactiveTime && State == 0) {
			ticker = 0;
			State = 1;
		} else if (State == 1) {
			this.GetComponent<SpriteRenderer> ().color = Color.Lerp (basecol, zerocol, ticker / TimeOut);
			if (ticker > TimeOut)
				Destroy (gameObject);
		}

		ticker += Time.unscaledDeltaTime;

	}
}
