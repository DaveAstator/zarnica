﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class zrEndEmitter : MonoBehaviour {
	public float TimeOut=3f;
	public float ticker=0f;

	public float ptTimeOut = 0.00001f;
	public float pticker = 0f;
	// Use this for initialization
	void Start () {
		float y=this.transform.position.y;
		float inc = 0.01f;
		int step = 0;
		while (y < Camera.main.transform.position.y + Screen.height / 200f) {
			GameObject EXP = (GameObject)Instantiate (Resources.Load ("prefabs/effects/zrEndParticle"));
			EXP.transform.position = new Vector3(this.transform.position.x,y,this.transform.position.z);//this.transform.position;
			EXP.transform.localScale = this.transform.localScale;
			EXP.transform.SetParent (Camera.main.transform);
			EXP.GetComponent<SpriteRenderer> ().color = Color.Lerp (new Color (1f, 1f, 1f, 1f), new Color (1f, 0f, 0f, 1f), step / 25f);
			EXP.GetComponent<FadeAway> ().InactiveTime = step/50f;
			step += 1;
			y = y + inc;
			inc += 0.01f;
		}





	}
	
	// Update is called once per frame
	void Update () {
		/*
		ticker += Time.unscaledDeltaTime;
		pticker += Time.unscaledDeltaTime;
	//	if (pticker > ptTimeOut) {
			GameObject EXP = (GameObject)Instantiate (Resources.Load ("prefabs/effects/zrEndParticle"));
			EXP.transform.position = this.transform.position;//this.transform.position;
			EXP.transform.localScale = this.transform.localScale;
			EXP.transform.SetParent (Camera.main.transform);
			EXP.GetComponent<SpriteRenderer> ().color = Color.Lerp (new Color (1f, 1f, 1f, 1f), new Color (1f, 0f, 0f, 1f), ticker / TimeOut);
			pticker = pticker-ptTimeOut;
		//}
		if (ticker > TimeOut)
			Destroy (gameObject);
*/
	}
}
