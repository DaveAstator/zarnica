﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LutAnimator : MonoBehaviour {
	//public Texture2D LookupTexture1;
	//public Texture2D LookupTexture2;
	// Use this for initialization
	public DigitalRuby.SimpleLUT.SimpleLUT Lut1;
	public DigitalRuby.SimpleLUT.SimpleLUT Lut2;
	public float time1end = 3f;
	public float time2start= 4f;
	public float time2end = 4f;
	private float ticker=0f;
	private int fl =0;
	private int state=0;
	void Start () {



	}

	// Update is called once per frame
	void Update () {
		if (state == 0) {
			Lut1.Amount = (Mathf.Min(ticker,time1end) / time1end)/1f;
			Lut2.Amount = Mathf.Min (Mathf.Max (ticker - time2start, 0f) / time2end, 1f);
		}
			ticker += Time.unscaledDeltaTime;
	}

}
