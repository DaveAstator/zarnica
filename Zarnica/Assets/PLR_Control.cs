﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PLR_Control : MonoBehaviour {

	public
	float Speed_KB;
	float csX,csY;

	int isfiring=0;
	// Use this for initialization

	public static float RoundToNearestPixel(float unityUnits)
	{
		//float valueInPixels = (Screen.height / (viewingCamera.orthographicSize * 2)) * unityUnits;
		float ors= Screen.height / 2.0f / 100.0f;
		float valueInPixels = (Screen.height / (ors * 2)) * unityUnits;
		valueInPixels = Mathf.Round(valueInPixels);
		float adjustedUnityUnits = valueInPixels / (Screen.height / (ors * 2));
		return adjustedUnityUnits;
	}

	void Start () {
		csX = this.transform.position.x;
		csY = this.transform.position.y;
		//vx = 0;
		//vy = 0;
	}
	
	// Update is called once per frame
	void Update () {
		var BTV = GetComponent<BaseValues>();
		csX = this.transform.position.x;
		csY = this.transform.position.y;

		if (Input.GetKey (KeyCode.LeftArrow))
			BTV.Vel.x  = -Speed_KB;
		else if (Input.GetKey (KeyCode.RightArrow))
			BTV.Vel.x  = Speed_KB;
		else 
			BTV.Vel.x  = 0;


		if (Input.GetKey (KeyCode.UpArrow))
			BTV.Vel.y = Speed_KB;
		else if (Input.GetKey (KeyCode.DownArrow))
			BTV.Vel.y = -Speed_KB;
		else
			BTV.Vel.y = 0;

		/*

		if (Input.GetKeyDown(KeyCode.LeftArrow))
		{
			BTV.Vel.x  -= Speed_KB;
		}
		if (Input.GetKeyDown(KeyCode.RightArrow))
		{
			BTV.Vel.x   += Speed_KB;
		}
		if (Input.GetKeyDown(KeyCode.UpArrow))
		{
			BTV.Vel.y += Speed_KB;
		}
		if (Input.GetKeyDown(KeyCode.DownArrow))
		{
			BTV.Vel.y -= Speed_KB;
		}
		//----------------cancels
		if (Input.GetKeyUp(KeyCode.LeftArrow))
		{
			BTV.Vel.x += Speed_KB;
		}
		if (Input.GetKeyUp(KeyCode.RightArrow))
		{
			BTV.Vel.x  -= Speed_KB;
		}
		if (Input.GetKeyUp(KeyCode.UpArrow))
		{
			BTV.Vel.y -= Speed_KB;
		}
		if (Input.GetKeyUp(KeyCode.DownArrow))
		{
			BTV.Vel.y += Speed_KB;
		}

*/


		if (Input.GetKeyUp(KeyCode.D))
		{
		//	Quaternion rot = Quaternion.Euler (0, 0, Camera.main.transform.localRotation.z+15f);
		//	Camera.main.transform.localRotation = rot;
		}


		if (this.transform.localPosition.x > Screen.width / 200f - 32/100f)
			GetComponent<BaseValues> ().LocalMovPosition = new Vector3 ((Screen.width / 200f) - 32/100f, GetComponent<BaseValues> ().LocalMovPosition.y, GetComponent<BaseValues> ().LocalMovPosition.z);

		if (this.transform.localPosition.x < -Screen.width / 200f + 32/100f)
			GetComponent<BaseValues> ().LocalMovPosition = new Vector3 (-(Screen.width / 200f) + 32/100f, GetComponent<BaseValues> ().LocalMovPosition.y, GetComponent<BaseValues> ().LocalMovPosition.z);

		if (this.transform.localPosition.y > Screen.height / 200f - 32/100f)
			GetComponent<BaseValues> ().LocalMovPosition = new Vector3 ( GetComponent<BaseValues> ().LocalMovPosition.x,(Screen.height / 200f) - 32/100f, GetComponent<BaseValues> ().LocalMovPosition.z);

		if (this.transform.localPosition.y < -Screen.height / 200f + 32/100f)
			GetComponent<BaseValues> ().LocalMovPosition = new Vector3 ( GetComponent<BaseValues> ().LocalMovPosition.x,-(Screen.height / 200f) + 32/100f, GetComponent<BaseValues> ().LocalMovPosition.z);
		
		//GameObject EXP = Resources.Load("EffExplosion")
	//	GameObject EXP = (GameObject)Instantiate(Resources.Load("EffExplosion"));
//		EXP.transform.position = this.transform.position;

//		csX = csX + BTV.vx;
	//	csY = csY + BTV.vy;
		//this.transform.position = new Vector3 (RoundToNearestPixel(csX),RoundToNearestPixel(csY), this.transform.position.z);

	}
}
