﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SPWN_single : MonoBehaviour {
	[Tooltip("1= ahead -1=behind")]
	public float SpawnShift = 1;
	public bool AttachToCam =false;


	public
	string ObjName="prefabs/effects/enemyexplosion";

	// Use this for initialization
	void Start () {
		GetComponent<SpriteRenderer> ().enabled = false;
	}
	
	// Update is called once per frame
	void Update () {
		if ((this.transform.position.y-Camera.main.transform.position.y)<(2.90*SpawnShift)) {
			GameObject EXP = (GameObject)Instantiate(Resources.Load(ObjName));
			EXP.transform.position = this.transform.position;//this.transform.position;
			EXP.transform.rotation = this.transform.rotation;
			EXP.transform.localScale = this.transform.localScale;
			if (AttachToCam) EXP.transform.SetParent (Camera.main.transform);
			Destroy(this.gameObject);
		}
	}
	void OnDrawGizmos(){
		Gizmos.DrawLine (transform.position, new Vector3(transform.position.x,transform.position.y-0.5f*SpawnShift,transform.position.z));
	}
}
