﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MOV_ahead : MonoBehaviour {
	public float value=-0.05f;
	public float accel = 0f;
	public int phase=1;
	private
	BaseValues BV;
	// Use this for initialization
	void Start () {
	BV = this.GetComponent<BaseValues>();	
	}

	// Update is called once per frame
	void Update () {
		value += accel * Time.unscaledDeltaTime;
		BV.Vel.y = value;
		//this.transform.localPosition += this.transform.up * value;//new Vector3(this.transform.position.x,this.transform.position.y-0.05f,this.transform.position.z);

	}
}
