﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Cam_Space_Script : MonoBehaviour {

	public 
	float vx;
	float csX;
	float csY;
	// Use this for initialization
	public static float RoundToNearestPixel(float unityUnits)
	{
		//float valueInPixels = (Screen.height / (viewingCamera.orthographicSize * 2)) * unityUnits;
		float ors= Screen.height / 2.0f / 100.0f;
		float valueInPixels = (Screen.height / (ors * 2)) * unityUnits;
		valueInPixels = Mathf.Round(valueInPixels);
		float adjustedUnityUnits = valueInPixels / (Screen.height / (ors * 2));
		return adjustedUnityUnits;
	}

	void Start () {
		vx = 0.005f;
		csX = this.transform.position.x;
		csY = this.transform.position.y;
	}
	
	// Update is called once per frame
	void Update () {
		csX = csX + vx;
		//this.transform=Camera.main.transform
		this.transform.position = new Vector3 (RoundToNearestPixel(csX+vx),this.transform.position.y, this.transform.position.z);

		//this.transform.position=Camera.main.transform.position;
		//this.transform.rotation=Camera.main.transform.rotation;
		//this.transform.position+=this.transform.right*(csX); // = new Vector3 (RoundToNearestPixel(Camera.main.transform.position.x+csX), Camera.main.transform.position.y, this.transform.position.z);
		//this.transform.position = new Vector3 (RoundToNearestPixel(Camera.main.transform.position.x+csX), Camera.main.transform.position.y, this.transform.position.z);
	}
}
