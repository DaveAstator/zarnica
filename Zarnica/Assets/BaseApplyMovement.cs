﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BaseApplyMovement : MonoBehaviour {

	// Use this for initialization
	void Start () {
		
	}

	public static float RoundToNearestPixel(float unityUnits)
	{
		//float valueInPixels = (Screen.height / (viewingCamera.orthographicSize * 2)) * unityUnits;
		float ors= Screen.height / 2.0f / 100.0f;
		float valueInPixels = (Screen.height / (ors * 2)) * unityUnits;
		valueInPixels = Mathf.Round(valueInPixels);
		float adjustedUnityUnits = valueInPixels / (Screen.height / (ors * 2));
		return adjustedUnityUnits;
	}


	// Update is called once per frame
	void Update () {
		var BTV = GetComponent<BaseValues>();
		BTV.LocalMovPosition.x = this.transform.localPosition.x;
		BTV.LocalMovPosition.y = this.transform.localPosition.y;
		BTV.LocalMovPosition.x += BTV.Vel.x;
		BTV.LocalMovPosition.y += BTV.Vel.y;
		//BTV.Cz += BTV.vz;
		this.transform.localPosition = new Vector3 (RoundToNearestPixel(BTV.LocalMovPosition.x),RoundToNearestPixel(BTV.LocalMovPosition.y), this.transform.position.z);
	}
}
