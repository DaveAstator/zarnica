﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Cam_test_SCR : MonoBehaviour {
	public
	float FlySpeed;


	private Vector3 FullPos;
	public static float RoundToNearestPixel(float unityUnits, Camera viewingCamera)
    {
        float valueInPixels = (Screen.height / (viewingCamera.orthographicSize * 2)) * unityUnits;
        valueInPixels = Mathf.Round(valueInPixels);
        float adjustedUnityUnits = valueInPixels / (Screen.height / (viewingCamera.orthographicSize * 2));
        return adjustedUnityUnits;
    }
	

	// Use this for initialization
	void Start () {
		Camera.main.orthographicSize = Screen.height / 2.0f / 100.0f;
		FullPos = this.transform.position;
	}
	
	// Update is called once per frame
	void Update () {
		//Camera.current.transform.Translate(new Vector3(0.0f, 0.03f, 0.0f));
		//Camera.main.transform.position += Camera.main.transform.up * FlySpeed;
		FullPos = FullPos + Camera.main.transform.up * FlySpeed * Time.unscaledDeltaTime;

		//print ((Camera.main.transform.up * FlySpeed * Time.unscaledDeltaTime).y);
		//Vector3 roundPos = newPos;
		Vector3 roundPos = new Vector3(RoundToNearestPixel(FullPos.x, GetComponent<Camera>()),RoundToNearestPixel(FullPos.y, GetComponent<Camera>()),FullPos.z);

		Camera.main.transform.position = roundPos;//roundPos;



		//Camera.current.orthographicSize = Camera.current.orthographicSize - 0.005f;

	}
}
