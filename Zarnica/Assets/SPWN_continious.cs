﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SPWN_continious : MonoBehaviour {

	public
	int SpawnShift = 1;
	public float SpawnDelay = 24;
	public
	string ObjName="EnemyRocket1";
	public bool AttachToCam =false;
	private
	float Delay;
	// Use this for initialization
	void Start () {
		Delay = SpawnDelay;
	}

	// Update is called once per frame
	void Update () {
		if (Mathf.Abs(this.transform.position.y-Camera.main.transform.position.y)<(2.90*SpawnShift)) {
	
			Vector3 diff= this.transform.position-(GameObject.Find("zarnica").transform.position);
			diff.Normalize();
			float rotz = Mathf.Atan2 (diff.y, diff.x) * Mathf.Rad2Deg;
			//this.transform.rotation = Quaternion.Euler (0f,0f,rotz-90); 
			Quaternion aimrot = Quaternion.Euler (0f,0f,rotz-90);

			Delay--;
			if (Delay < 1) {
				GameObject EXP = (GameObject)Instantiate (Resources.Load (ObjName));
				EXP.transform.position = new Vector3(this.transform.position.x,this.transform.position.y,this.transform.position.z-1);//this.transform.position;

				EXP.transform.localRotation = aimrot;//this.transform.rotation;
				EXP.transform.localScale = this.transform.lossyScale;
				if (AttachToCam) 
					EXP.transform.SetParent (Camera.main.transform);

				Delay = (SpawnDelay/(GlobalDifficulty.K_EnmShootSpeed));


			}

			//EXP.transform.SetParent (Camera.main.transform);
			//Destroy(this.gameObject);
		}
	}
}
